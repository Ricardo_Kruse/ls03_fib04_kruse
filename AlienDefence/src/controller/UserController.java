package controller;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.UserDB;
/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private UserDB userDB;
	public UserController(UserDB userDB) {
		this.userDB = userDB;
	}
	
	public void createUser(User user) {
		
	//	if (user.getLoginname())
		userDB.createUser(user);
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username) {
		return userDB.readUser(username);
	}
	
	public void changeUser(User user) {
		userDB.updateUser(user);
	}
	
	public void deleteUser(User user) {
		userDB.deleteUser(user.getP_user_id());
	}
	
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username);
		if(user.getPassword().equals(passwort)) {
			return true;
		}
		else {
			return false;
		}
	}
}
