import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Form_aenderm extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aenderm frame = new Form_aenderm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aenderm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblNewLabel.setBounds(0, 44, 434, 14);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel);
		
		JButton btnHintergrundRot = new JButton("Rot");
		btnHintergrundRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnHintergrundRot.setBounds(10, 115, 110, 23);
		contentPane.add(btnHintergrundRot);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setBounds(10, 90, 223, 14);
		contentPane.add(lblAufgabeHintergrundfarbe);
		
		JButton btnHintergrundGrn = new JButton("Gr\u00FCn");
		btnHintergrundGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnHintergrundGrn.setBounds(161, 115, 110, 23);
		contentPane.add(btnHintergrundGrn);
		
		JButton btnHintergrundBlau = new JButton("Blau");
		btnHintergrundBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnHintergrundBlau.setBounds(314, 115, 110, 23);
		contentPane.add(btnHintergrundBlau);
		
		JButton btnHintergrundGelb = new JButton("Gelb");
		btnHintergrundGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnHintergrundGelb.setBounds(10, 149, 110, 23);
		contentPane.add(btnHintergrundGelb);
		
		JButton btnHintergrundStandardfarbe = new JButton("Standard");
		btnHintergrundStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color(240, 240, 240));
			}
		});
		btnHintergrundStandardfarbe.setBounds(161, 149, 110, 23);
		contentPane.add(btnHintergrundStandardfarbe);
		
		JButton btnHintergrundFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnHintergrundFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(JColorChooser.showDialog(null, "W�hlen Sie eine Farbe", Color.WHITE));
			}
		});
		btnHintergrundFarbeWhlen.setBounds(314, 149, 110, 23);
		contentPane.add(btnHintergrundFarbeWhlen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text Formatieren");
		lblAufgabeText.setBounds(10, 183, 191, 14);
		contentPane.add(lblAufgabeText);
		
		JButton btnSchriftart = new JButton("Arial");
		btnSchriftart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font ("Arial", Font.ITALIC, 11));
			}
		});
		btnSchriftart.setBounds(10, 211, 110, 23);
		contentPane.add(btnSchriftart);
		
		JButton btnSchriftart_1 = new JButton("Comic Sans MS");
		btnSchriftart_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font ("Comic Sans MS", Font.ITALIC, 11));
			}
		});
		btnSchriftart_1.setBounds(161, 211, 110, 23);
		contentPane.add(btnSchriftart_1);
		
		JButton btnSchriftart_2 = new JButton("Courier New");
		btnSchriftart_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font ("Curier New", Font.ITALIC, 11));
			}
		});
		btnSchriftart_2.setBounds(314, 211, 110, 23);
		contentPane.add(btnSchriftart_2);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 245, 414, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label Schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 276, 202, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnTextImLabel.setBounds(222, 276, 202, 23);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setBounds(10, 310, 414, 14);
		contentPane.add(lblAufgabeSchriftfarbe);
		
		JButton buttonSchriftRot = new JButton("Rot");
		buttonSchriftRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		buttonSchriftRot.setBounds(10, 335, 110, 23);
		contentPane.add(buttonSchriftRot);
		
		JButton buttonSchriftBlau = new JButton("Blau");
		buttonSchriftBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		buttonSchriftBlau.setBounds(161, 335, 110, 23);
		contentPane.add(buttonSchriftBlau);
		
		JButton btnSchriftSchwarz = new JButton("Schwarz");
		btnSchriftSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnSchriftSchwarz.setBounds(314, 335, 110, 23);
		contentPane.add(btnSchriftSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setBounds(10, 369, 414, 14);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton buttonSchriftGroesser = new JButton("+");
		buttonSchriftGroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
			}
		});
		buttonSchriftGroesser.setBounds(10, 394, 202, 23);
		contentPane.add(buttonSchriftGroesser);
		
		JButton buttonSchriftKleiner = new JButton("-");
		buttonSchriftKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
			}
		});
		buttonSchriftKleiner.setBounds(222, 394, 202, 23);
		contentPane.add(buttonSchriftKleiner);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(10, 428, 414, 14);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(10, 453, 110, 23);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(161, 453, 110, 23);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(314, 453, 110, 23);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 487, 414, 14);
		contentPane.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(10, 512, 414, 49);
		contentPane.add(btnExit);
	}
}
